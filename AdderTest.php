<?php
use PHPUnit\Framework\TestCase;

include 'Adder.php';

class AdderTest extends TestCase
{
  protected $adder;

  protected function setUp() {
    $this->adder = new Adder;
  }

  public function testAdderShouldExists() {
    $this->assertNotEmpty($this->adder);
  }

  /**
   * @expectedException TypeError
   **/
  public function testStringShouldNotOperate()
  {
    $this->adder->operate('s', 0);
    $this->adder->operate(0, 's');
  }

  public function testAdderShouldOperateFirstTwoArguments() {
    $this->assertEquals(2, $this->adder->operate(1, 1, 4));
    $this->assertEquals(0, $this->adder->operate(2, -2, 4, 5));
  }

}
